/* Set navigation */

function openNav() {
  $("#mySidenav").addClass("width80");
  $("#nav-res").addClass("opacityon");
  $(".cd-shadow-layer").addClass("displayblock");
  $(".wrapper").addClass("position-fixed");
  $("body").addClass("overflow-fixed");
}

function closeNav() {
  $("#mySidenav").removeClass("width80");
  $("#nav-res").removeClass("opacityon");
  $(".cd-shadow-layer").removeClass("displayblock");
  $(".wrapper").removeClass("position-fixed");
  $("body").removeClass("overflow-fixed");
} 


$(document).ready(function(){ 

  $(".cd-shadow-layer").click(function(){
    closeNav(); 
  });

  // Waves.attach('.btn-waves', ['waves-effect']);

  $(window).scroll(function() {
    if ($(this).scrollTop() >= 350) {        
        $('.return-to-top').addClass("display_show");    
    } else {
        $('.return-to-top').removeClass("display_show");   
    }
  });

  $('.return-to-top').click(function() {    
    $('body,html').animate({
        scrollTop : 0                       
    }, 0);
  });

  $(window).scroll(function(){
    var sticky = $('.header-div'),
        scroll = $(window).scrollTop();
  
      if (scroll >= 200) sticky.addClass('header-bgcolor');
      else sticky.removeClass('header-bgcolor');

  });

  
});


/* end of navigation */